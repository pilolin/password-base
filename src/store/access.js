import Vue from 'vue'
export default {
  state: {
    accesses: []
  },
  mutations: {
    SET_ACCESSES(state, payload) {
      state.accesses = payload
    },
  },
  getters: {
    getAccesses: state => {
      return state.accesses;
    }
  },
  actions: {
    LOAD_ACCESSES({commit}, payload) {
      commit('SET_PROCESSING', true);
      Vue.$db.collection('accesses')
        .where('user', '==', payload.user)
        .get()
        .then(querySnapshot => {
          let accesses = [];
          querySnapshot.forEach(a => {
            const data = a.data();
            let access = {
              id: a.id,
              title: data.title,
              user: data.user,
              extraField: data.extraField,
              description: data.description,
              data: data.data
            }

            accesses.push(access);
          });
          
          commit('SET_ACCESSES', accesses);
          commit('SET_PROCESSING', false);
        })
        .catch(error => {
          commit('SET_PROCESSING', false);
          commit('SET_ERROR', error.message);
        })
    },
    ADD_ACCESSES({commit}, payload) {
      commit('SET_PROCESSING', true);
      Vue.$db.collection('accesses')
        .add(payload)
        .then(docRef => {
          console.log("Document written with ID: ", docRef.id);
          commit('SET_PROCESSING', false);
        })
        .catch(error => {
          commit('SET_PROCESSING', false);
          commit('SET_ERROR', error.message);
        })
    }
  }
}